import React from 'react';
import ReactDOM from 'react-dom';
import WebFont from 'webfontloader';
import config from 'visual-config-exposer';

import App from './app';

WebFont.load({
  google: {
    families: [config.settings.fontType, 'Sans Serif'],
  },
});

import './index.css';

ReactDOM.render(<App />, document.getElementById('root'));
